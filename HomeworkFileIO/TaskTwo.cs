﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HomeworkFileIO
{
    public abstract class TaskTwo
    {
        public static void AdditionOfNumbers(string pathToFile)
        {
            int firstNumber;
            int secondNumber;

            using(FileStream stream = File.OpenRead($"{pathToFile}\\input.txt"))
            {
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, data.Length);
                int.TryParse(Encoding.Default.GetString(data).Split(' ')[0].ToString(), out firstNumber);
                int.TryParse(Encoding.Default.GetString(data).Split(' ')[1].ToString(), out secondNumber);
            }

            using(FileStream stream = File.Create($"{pathToFile}\\output.txt"))
            {
                byte[] data = new byte[stream.Length];
                data = Encoding.Default.GetBytes((firstNumber + secondNumber).ToString());
                stream.Write(data,0,data.Length);
            }
        }
    }
}
