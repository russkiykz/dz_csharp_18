﻿using System.IO;
using System.Text;

namespace HomeworkFileIO
{
    public abstract class TaskOne
    {
        public static int Fibonacci(int n)
        {
            return n > 1 ? Fibonacci(n - 1) + Fibonacci(n - 2) : n;
        }

        public static void FibonacciNumbersToFile(string pathToFile)
        {
            int amountOfNumbers;
            using (FileStream stream = File.OpenRead($"{pathToFile}\\Fibonacci.txt"))
            {
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, data.Length);
                amountOfNumbers = Encoding.Default.GetString(data).Split(' ').Length;
            }

            using (FileStream stream = File.OpenWrite($"{pathToFile}\\Fibonacci.txt"))
            {
                byte[] data = new byte[stream.Length];
                string rowFibonacci = "";
                for (int i = 0; i < amountOfNumbers * 2; i++)
                {
                    if(i!= amountOfNumbers * 2 - 1)
                    {
                        rowFibonacci += $"{Fibonacci(i)} ";
                    }
                    else
                    {
                        rowFibonacci += $"{Fibonacci(i)}";
                    }
                    
                }
                data = Encoding.Default.GetBytes(rowFibonacci);
                stream.Write(data, 0, data.Length);
            }
        }

    }
}
